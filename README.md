A sample project for practicing animation using scene with Transition framework.
a. Demonstrated the animating the transition between the scenes represented by two layout resource files.
b. Used a transition XML resource file to configure the transition animation effects between the two scenes.
