package client.mytango.com.scenetransition;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.transition.Scene;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;


public class MyActivity extends Activity {

    ViewGroup rootContainer;
    Scene scene1;
    Scene scene2;
    Transition transitionMgr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            rootContainer = (ViewGroup) findViewById(R.id.rootContainer);

            transitionMgr = TransitionInflater.from(this).inflateTransition(R.transition.transition);

            scene1 = Scene.getSceneForLayout(rootContainer, R.layout.scene1_layout, this);

            scene2 = Scene.getSceneForLayout(rootContainer,
                    R.layout.scene2_layout, this);

            scene1.enter();
        }
    }

    public void goToScene2 (View view)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            TransitionManager.go(scene2, transitionMgr);
        }
    }

    public void goToScene1 (View view)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            TransitionManager.go(scene1, transitionMgr);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
